# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, In

class ContactMechanism(ModelSQL, ModelView):
    _name = "party.contact_mechanism"

    intended_use = fields.Selection([
             ('home', 'Home'),
             ('work', 'Work'),
             ('other', 'Other'),
        ], 'Intended Use', states={
            'invisible': ~In(Eval('type'), ['phone', 'email', 'fax', 'website']),
        }, depends=['type'])

ContactMechanism()

