# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Contact Mechanism Intended Use',
    'name_de_DE': 'Parteien Kontaktmöglichkeit Verwendungszweck',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds support of intended use to contact mechanisms of parties
    ''',
    'description_de_DE': '''
    - Fügt Unterstützung von Verwendungszweck für Kontaktmöglichkeiten von
    Parteien hinzu.
    ''',
    'depends': [
        'party'
    ],
    'xml': [
        'party.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
